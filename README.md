Maintainer
----------

To build the package, download the asix sources in the root directory of this repository, and rename the file.

```bash
mv asix.tgz asix_0.1.4.orig.tar.gz
./build-package.sh
```

Compile against another distribution
------------------------------------

```bash
mk-sbuild --arch=amd64 --name=bionic bionic
./build-package.sh setup
cd asix
sbuild -d bionic-amd64
```
