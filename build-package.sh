#!/bin/bash

set -euo pipefail

declare -r VERSION='0.1.4-2'
declare -r ORIG_VERSION='0.1.4'
declare -r ORIG_FILE="asix_$ORIG_VERSION.orig.tar.gz"

if [[ ! -r $ORIG_FILE ]]; then
  echo "Cannot source file $ORIG_FILE!" >&2
  exit 1
fi

rm -rf asix
tar xf $ORIG_FILE
cp -r debian asix/debian
cd asix

if [[ $# -eq 0 ]]; then
  debuild -us -uc
else
  echo 'Created working directory'
fi

